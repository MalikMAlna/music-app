// Cached Variables Here

// TODO: All the logic for the buttons to work

window.addEventListener('load', () => {
    const sounds = document.querySelectorAll('.sound');
    const pads = document.querySelectorAll(".pads div");
    const visual = document.querySelector(".visual");
    const colors = [
        "#60d394",
        "#d36060",
        "#c060d3",
        "#d3d160",
        "#606bd3",
        "#60c2d3",
        "#8A2BE2"
    ];

    // Here's the sound
    pads.forEach((pad, idx) => {
        pad.addEventListener("click", ()=>{
            sounds[idx].currentTime = 0;
            sounds[idx].play();
            createBubbles(idx);
        });
    });
    // Bubbles function
    const createBubbles = (idx) => {
        const bubble = document.createElement("div");
        visual.appendChild(bubble);
        bubble.style.backgroundColor = colors[idx];
        bubble.style.animation = "jump 1s ease";
        // Remove animated bubbles to prevent lag
        bubble.addEventListener('animationend', function(){
            visual.removeChild(this);
        });
    }
});